from distutils.core import setup
setup(
    name='stepper',
    version='1.0',
    description='stepper library',
    author='hgcserver',
    author_email='594352301@qq.com',
    url='https://gitee.com/hgcserver',
    py_modules=['stepper']
)