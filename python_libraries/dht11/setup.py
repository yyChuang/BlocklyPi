from distutils.core import setup
setup(
    name='dht11',
    version='1.0',
    description='dht11 library',
    author='hgcserver',
    author_email='594352301@qq.com',
    url='https://gitee.com/hgcserver',
    py_modules=['dht11']
)